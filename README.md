# Let's Find Out What's Special About Rust!

Talk given at the live@Manning Rust conference 2020-09-15.

If you want to learn more about Rust, I recommend:

* [Rust in Motion](https://www.manning.com/livevideo/rust-in-motion?a_aid=cnichols&a_bid=6a993c2e) liveVideo series
* [The Rust Programming Language book](https://doc.rust-lang.org/stable/book/) free to read online, available in print from [No Starch Press](https://nostarch.com/Rust2018)
* [Awesome Rust](https://github.com/rust-unofficial/awesome-rust), a curated list of resources
* [This Week in Rust](https://this-week-in-rust.org/), a weekly email newsletter of the latest blog posts and developments in Rust

Official forums where you can interact with the Rust community:

* [The users forum](https://this-week-in-rust.org/) for discussing using Rust
* [The internals forum](https://internals.rust-lang.org/) about developing Rust itself
* [The official Discord chat server](https://discord.gg/rust-lang), especially check out the #beginners room if you're getting started

Some topics I referenced that you might want more information about:

* [Microsoft, Feb 2019: 70% of all security bugs in the last 12 years are memory safety issues](https://www.zdnet.com/article/microsoft-70-percent-of-all-security-bugs-are-memory-safety-issues/)
* [Chrome, May 2020: 70% of all security bugs in the last 5 years are memory safety issues](https://www.zdnet.com/article/chrome-70-of-all-security-bugs-are-memory-safety-issues/)
* [Why the C Language Will Never Stop You from Making Mistakes](https://thephd.github.io/your-c-compiler-and-standard-library-will-not-help-you) - 2020-08-09, JeanHeyd Meneide
* [Esteban Kuber](https://twitter.com/ekuber) has done the most work on improving the Rust compiler error messages and did [a talk at RustConf about them](https://www.youtube.com/watch?v=Z6X7Ada0ugE)
* [Rust RFCs](https://github.com/rust-lang/rfcs) - our open decision making process
